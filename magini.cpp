//
// Author:      Kevin Ingles
// File:        magini.cpp
// Description: A header-only C++ project building framework, source file
//
// Copyright (c) 2023 Kevin Ingles

// #define MAGINI_COMPILER "clang++"
#define MAGINI_COMPILER "g++"
#define MAGINI_SAVE_OUTPUT
#define MAGINI_IMPLEMENTATION
#include "ThirdParty/magini/magini.hpp"

#include <iostream>
#include <stdio.h>
#include <string>

using namespace magini;

int main([[maybe_unused]] int argc, char** argv)
{
    MAGINI_INITIATE_LOGGING;
    print_disclaimer();
    REBUILD_MAGINI(argc, argv);

    auto cwd        = std::filesystem::current_path();
    auto cwd_string = cwd.string();

    BuildTree build_tree;
    build_tree.has_libraries = false;
    build_tree.source_files.push_back({ cwd / "EntryPoint.cpp", FileStatus::NOT_CHECKED });
    TRY_MAIN(add_directory_build_tree(build_tree, (cwd / "ThirdParty/fmt")));

    //    auto path_to_core    = (cwd / "Core").string();
    // auto path_to_utility = (cwd / "Utility").string();
    // auto path_to_fmt     = (cwd / "ThirdParty" / "fmt").string();

    // ProtoCommand include_flags = { "-I", path_to_core, "-I", path_to_utility, "-I", path_to_fmt
    // };

    ProtoCommand include_flags = { "-I", cwd_string };
    output_compile_commands(build_tree, {}, include_flags, {});

    auto executable = TRY_MAIN(build(build_tree, {}, include_flags, {}));

    TRY_MAIN(execute({ executable.c_str() }));
    return 0;
}
