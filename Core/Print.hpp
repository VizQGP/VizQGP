//      VizQGP: A software package for visualizing high-energy physics phenomena
//      Copyright (C) 2023  Kevin Ingles
//
//      This program is free software: you can redistribute it and/or modify it under the terms of
//      the GNU General Public License as published by the Free Software Foundation, either version
//      3 of the License, or (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//      without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//      See the GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License along with this program.
//      If not, see <http://www.gnu.org/licenses/>.
//
// ------------------------------------------------------------------------------------------------
// Author: Kevin Ingles
// File: Print.hpp
// Description: Utility functions to facilitate different types of printing

#ifndef VIZQGP_PRINT_HPP
#define VIZQGP_PRINT_HPP

#include "ThirdParty/fmt/format.h"
#include "ThirdParty/fmt/printf.h"
#include "Utility/ErrorHandling.hpp"

#include <utility>

namespace vizqgp {

    template <template <typename...> class FmtString, class... Args>
    void Print(FmtString<Args...>&& fmt_string, Args&&... args)
    {
        fmt::print(fmt_string, std::forward<Args>(args)...);
    }

    template <template <typename...> class FmtString, class... Args>
    void PrintLn(FmtString<Args...>&& fmt_string, Args&&... args)
    {
        fmt::println(fmt_string, std::forward<Args>(args)...);
    }

}    // namespace vizqgp

#endif
