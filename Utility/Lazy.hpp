//      VizQGP: A software package for visualizing high-energy physics phenomena
//      Copyright (C) 2023  Kevin Ingles
//
//      This program is free software: you can redistribute it and/or modify it under the terms of
//      the GNU General Public License as published by the Free Software Foundation, either version
//      3 of the License, or (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//      without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//      See the GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License along with this program.
//      If not, see <http://www.gnu.org/licenses/>.
//
// ------------------------------------------------------------------------------------------------
// Author: Kevin Ingles
// File: Lazy.hpp
// Description: Data structure that handles lazy evaluating

#ifndef VIZQGP_LAZY_HPPP
#define VIZQGP_LAZY_HPPP

#include <functional>
#include <type_traits>
#include <utility>

namespace vizqgp {

    /// Implementation of a lazy function evaluator
    ///
    /// @brief Lazily evaluated functions, sometimes called thunks, can be created with arbitrarily
    /// many function parameters, and evaluated using the `*` or `()` operators
    ///
    /// @tparam Function the function to be called
    /// @tparam Args variadic template parameter that captures all arguments passed to be passed to
    /// function
    template <typename Function, class... Args>
    struct [[nodiscard]] Lazy {
        using ReturnType = std::invoke_result_t<Function, Args...>;

        Lazy() = default;

        Lazy(Function f_, Args&&... args_)
            : f{ f_ }
            , args{ std::forward<Args>(args_)... }
        {
        }

        constexpr explicit(true) operator bool() noexcept
        {
            optional = call(args);
            return optional.has_value();
        }

        constexpr auto operator*() { return *optional; }

        constexpr auto operator()() { return *optional; }

        template <class Tuple, size_t... Is>
        constexpr ReturnType _call(const Tuple& t, std::index_sequence<Is...>)
        {
            return std::invoke(f, std::get<Is>(t)...);
        }

        template <class... T>
        constexpr ReturnType call(const std::tuple<T...>& t)
        {
            return _call(t, std::index_sequence_for<T...>{});
        }

        Function            f{};
        std::tuple<Args...> args{};
        ReturnType          optional;
    };
}    // namespace vizqgp

// magini test begin
#ifdef MAGINI_TEST
#  include <cassert>
#  include <optional>

using namespace vizqgp;

inline void test_1()
{
    auto print_int = [](int a)
    {
        return std::optional<int>(a);
    };
    auto lazy_eval = Lazy(print_int, 10);
    assert(*lazy_eval == 10);
}
#endif
// magini test end

#endif
