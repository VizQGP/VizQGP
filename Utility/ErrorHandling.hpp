//      VizQGP: A software package for visualizing high-energy physics phenomena
//      Copyright (C) 2023  Kevin Ingles
//
//      This program is free software: you can redistribute it and/or modify it under the terms of
//      the GNU General Public License as published by the Free Software Foundation, either version
//      3 of the License, or (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//      without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//      See the GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License along with this program.
//      If not, see <http://www.gnu.org/licenses/>.
//
// ------------------------------------------------------------------------------------------------
// Author: Kevin Ingles
// File: ErrorHandling.hpp
// Description: Provide monadic and applicative methods for clean error handling

#ifndef VIZQGP_ERROR_HANDLING_HPP
#define VIZQGP_ERROR_HANDLING_HPP

#include "Core/Print.hpp"
#include "Utility/Lazy.hpp"

namespace vizqgp {

}

#endif
